<?php
/**
 * Created by PhpStorm.
 * User: Rakibul Hasan
 * Date: 5/14/2016
 * Time: 6:50 PM
 */

namespace App;


class Device
{
    const BLUEBITE_COOKIE = 'BLUEBITE_COOKIE';


    public static function setCookie($value, $expire = 0)
    {
        setcookie(self::BLUEBITE_COOKIE, $value, $expire);
    }

    public static function removeCookie(){
        unset($_COOKIE[self::BLUEBITE_COOKIE]);
        setcookie(self::BLUEBITE_COOKIE, '', time()-3600);
    }

    public static function getCookie()
    {
        if (isset($_COOKIE[self::BLUEBITE_COOKIE])) {
            return $_COOKIE[self::BLUEBITE_COOKIE];
        }
        return null;
    }
}